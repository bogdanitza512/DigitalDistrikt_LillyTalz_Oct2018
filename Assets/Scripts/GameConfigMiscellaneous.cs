﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.ConfigMiscellaneous
{
    [System.Serializable]
    public struct AnswearVariantContent
    {
        public string answearVariantText;
        public bool isCorrect;
    }

    [System.Serializable]
    public struct QuizzQuestionContent
    {
        public string questionText;
        public List<AnswearVariantContent> answearVariants;

        public AnswearVariantContent this[int i]
        {
            get { return answearVariants[i]; }
            set { answearVariants[i] = value; }
        }
    }

    [System.Serializable]
    public struct CardQA
    {
        public string question;
        public string answear;
    }


    [System.Serializable]
    public struct GameSpecs
    {
        public List<string> keyWords;
        public List<QuizzQuestionContent> quizzQuestions;
        public List<CardQA> cardQA;
        public List<QuizzQuestionContent> treatmentQuestions;
    }
}

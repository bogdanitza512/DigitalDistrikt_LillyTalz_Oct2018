﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using Sirenix.OdinInspector;
using UnityEngine;
using Game.ConfigMiscellaneous;

/// <summary>
/// 
/// </summary>
[CreateAssetMenu(fileName = "GameConfig_", menuName = "Custom/GameConfig", order = 1)]
public class GameConfig: SerializedScriptableObject
{

    #region Nested Types

    #endregion

    #region Fields and Properties

    [SerializeField]
    GameSpecs specs;

    public List<string> Keywords
    {
        get
        {
            CheckIfHasBeenDeserialized();
            return specs.keyWords;
        }
    }

    public List<QuizzQuestionContent> QuizzQuestions
    {
        get 
        {
            CheckIfHasBeenDeserialized();
            return specs.quizzQuestions; 
        }
    }

    public List<CardQA> CardQA
    {
        get
        {
            CheckIfHasBeenDeserialized();
            return specs.cardQA;
        }
    }

    public List<QuizzQuestionContent> TreatmentQuestions
    {
        get
        {
            CheckIfHasBeenDeserialized();
            return specs.treatmentQuestions;
        }
    }


    public bool hasBeenDeserialized = false;

    string PathToSpecsInJSON
    {
        get { return Path.Combine(Application.streamingAssetsPath, name + ".json"); }
    }

    #endregion

    #region Methods

    [Button("Save Specs To JSON", ButtonSizes.Medium)]
    public void Serialize()
    {
        string specsAsJSON = JsonUtility.ToJson(specs, true);
        File.WriteAllText(PathToSpecsInJSON, specsAsJSON);
        // Debug.Log(specsAsJSON);
    }

    [Button("Load Specs From JSON", ButtonSizes.Medium)]
    public void Deserialize()
    {
        if (File.Exists(PathToSpecsInJSON))
        {
            string dataAsJSON = File.ReadAllText(PathToSpecsInJSON);
            JsonUtility.FromJsonOverwrite(dataAsJSON, specs);
            // Debug.Log(dataAsJSON);
        }
    }

    public void CheckIfHasBeenDeserialized()
    {
        if (!hasBeenDeserialized)
        {
            hasBeenDeserialized = true;
            Deserialize();
        }
    }


    #endregion

}

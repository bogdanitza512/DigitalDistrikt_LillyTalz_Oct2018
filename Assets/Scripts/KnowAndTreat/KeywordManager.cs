﻿using System.Collections;
using System.Collections.Generic;
using BN512.Extensions;
using Sirenix.OdinInspector;
using UnityEngine;
using Keyword.Miscellaneous;
using System.Linq;
using System;

/// <summary>
/// 
/// </summary>
public class KeywordManager : SerializedMonoBehaviour {

    #region Nested Types



    #endregion

    #region Fields and Properties

    public KnowAndTreatGameController gameCtrl;

    public List<LetterBox> letterBoxes;

    public float baseTransitionDuration;

    public Dictionary<State, StateSpecs> transitionDict =
        new Dictionary<State, StateSpecs>()
    {
        {State.Correct,new StateSpecs(1,Color.green,Color.white,Color.black,1.0f)},
        {State.Hidden,new StateSpecs(1,Color.white,Color.white,Color.white.WithAlpha(0),1.0f)}
    };

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
        if(letterBoxes == null || letterBoxes.Count == 0)
        {
            AutoPopulateFields();
        }
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    [Button(ButtonSizes.Medium)]
	public void AutoPopulateFields()
    {
        letterBoxes.Clear();
        foreach (Transform child in transform)
        {
            var letterBox = child.GetComponent<LetterBox>();
            if (letterBox != null)
            {
                letterBoxes.Add(letterBox);
                letterBox.keywordManager = this;
            }
        }

    }

    [Button(ButtonSizes.Medium)]
    public void LoadRandomValidWord()
    {
        gameCtrl.keywordIndex = gameCtrl.config.Keywords.GetRandomIndex();
        print("Keyword: {0} of Lenght: {1}".FormatWith(gameCtrl.Keyword,gameCtrl.Keyword.Length));

        //AutoPopulateFields();

        foreach (var letterBox in letterBoxes)
        {
            var index = letterBoxes.IndexOf(letterBox);
            if (index < gameCtrl.Keyword.Length)
            {
                letterBox.Initialize(gameCtrl.Keyword[index], State.Hidden);
                //print("Hidden {0} with letter {1}".FormatWith(letterBox.name, gameCtrl.Keyword[index]));
            }else
            {
                letterBox.MakeInvisible();
                //print("Removed {0}".FormatWith(letterBox.name));
            }
        }

    }

    internal void TransitioAllLetterBoxesTo(State state)
    {
        foreach(var box in letterBoxes)
        {
            box.TransitionTo(state);
        }
    }

    public void Reset()
    {
        foreach (var letterBox in letterBoxes)
            letterBox.Reset();
        LoadRandomValidWord();
    }

    public void RevealRandomLetter()
    {
        letterBoxes.Where((box) => box.currentState == State.Hidden &&
                                   box.isInvisible == false)
                   .ToList()
                   .GetRandomItem()
                   .TransitionTo(State.Correct);
    }

    public void RevealLetter(LetterBox box)
    {
        if(letterBoxes.Contains(box))
        {
            box.TransitionTo(State.Correct);
        }
    }

    #endregion

}

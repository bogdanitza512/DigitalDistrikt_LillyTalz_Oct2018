﻿using System.Collections;
using System.Collections.Generic;
using BN512.Extensions;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
using System;
using System.Linq;
using Keyword.Miscellaneous;


/// <summary>
/// 
/// </summary>
public class PuzzlePiece : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{


    #region Nested Types

    public enum InteractionPhase { NoInteraction, BeginDrag, Dragging, EndDrag }

    #endregion

    #region Fields and Properties

    public PuzzleManager puzzleManager;

    public CanvasGroup canvasGroup;

    public string content;

    public TMP_Text letter;

    public InteractionPhase interactionPhase;

    public bool hasBeenMatched = false;

    public bool isAssigned = false;

    private LetterBox targetLetterBox;

    public string Content
    {
        get
        {
            return letter.text;
        }

        set
        {
            content = value;
            letter.text = value;
        }
    }

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {

    }

    #endregion

    #region Methods

    Vector3 initalPosition = Vector3.zero;

    void IBeginDragHandler.OnBeginDrag(PointerEventData eventData)
    {
        if (interactionPhase == InteractionPhase.NoInteraction &&
            hasBeenMatched == false)
        {
            interactionPhase = InteractionPhase.BeginDrag;
            transform.SetSiblingIndex(transform.parent.childCount - 1);
            if (initalPosition.Equals(Vector3.zero))
            {
                initalPosition = transform.position;
            }
        }
    }

    void IDragHandler.OnDrag(PointerEventData eventData)
    {
        if (interactionPhase == InteractionPhase.BeginDrag)
            interactionPhase = InteractionPhase.Dragging;
        if (interactionPhase == InteractionPhase.Dragging)
        {
            var mouseScreenPos =
                Input.mousePosition.WithZ(puzzleManager.masterCanvas.planeDistance);
            
            var mouseWorldPos =
                puzzleManager.masterCanvas.worldCamera.ScreenToWorldPoint(mouseScreenPos);

            transform.position = mouseWorldPos;

            var nearLetterBoxes = NearLetterBoxes;

            var target =
                nearLetterBoxes.Find((LetterBox box) => box.Content == Content);

            if (target != null)
            {
                transform.DOMove(target.transform.position,
                                 puzzleManager.baseTweenDuration);
                canvasGroup.DOFade(0,
                                   puzzleManager.baseTweenDuration * .75f);
                puzzleManager.gameCtrl.Notify_LetterMatch(target);
                interactionPhase = InteractionPhase.NoInteraction;
                hasBeenMatched = true;
                print("{0} is a match".FormatWith(target.name));
            }
            else
            {
                foreach (var box in nearLetterBoxes)
                {
                    box.TransitionTo(State.Incorect, false);
                    print("Not a match");
                }
            }

        }
    }

    public List<LetterBox> NearLetterBoxes
    {
        get
        {
            return puzzleManager
                         .gameCtrl
                         .keywordManager
                         .letterBoxes
                         .Where((box) => box.currentState == State.Hidden)
                         .Where((box) => Vector3.Distance(box.transform.position, transform.position) <= puzzleManager.snapThreshold)
                         .ToList();
        }
    }

    void IEndDragHandler.OnEndDrag(PointerEventData eventData)
    {
        if (interactionPhase == InteractionPhase.Dragging)
        {
            interactionPhase = InteractionPhase.EndDrag;

            var nearLetterBoxes = NearLetterBoxes;

            var target =
                nearLetterBoxes.Find((LetterBox box) => box.Content == Content);

            if (target != null)
            {
                transform.DOMove(target.transform.position,
                                 puzzleManager.baseTweenDuration);
                canvasGroup.DOFade(0,
                                   puzzleManager.baseTweenDuration * .75f);

                puzzleManager.gameCtrl.Notify_LetterMatch(targetLetterBox);

                hasBeenMatched = true;

            }
            else
            {
                transform.DOMove(initalPosition,
                                 puzzleManager.baseTweenDuration);
            }

            interactionPhase = InteractionPhase.NoInteraction;
        }

    }

    public void Assign(LetterBox targetBox)
    {
        isAssigned = true;
        targetLetterBox = targetBox;
        Content = targetBox.Content;
    }

    public void Assign(string newLetter)
    {
        isAssigned = true;
        Content = newLetter;
    }

    [Button(ButtonSizes.Medium)]
    void AutoPopulateFields()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        foreach (Transform child in transform)
        {
            var text = child.GetComponent<TMP_Text>();
            if (text != null)
                letter = text;
        }
    }

    public void Init(Transform parent)
    {
        transform.SetParent(parent);
        if (isAssigned)
        {
            initalPosition = transform.position;
        }

    }

    #endregion
}

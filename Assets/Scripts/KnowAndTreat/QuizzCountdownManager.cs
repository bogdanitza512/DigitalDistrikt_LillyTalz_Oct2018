﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using DG.Tweening;
using BN512.Extensions;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;
using System;


/// <summary>
/// 
/// </summary>
public class QuizzCountdownManager : MonoBehaviour {

    #region Nested Types



    #endregion

    #region Fields and Properties

    public KnowAndTreatGameController gameCtrl;

    [SerializeField]
    Image timeBarGreen;

    [SerializeField]
    Image timeBarGrey;

    [SerializeField]
    TMP_Text timeLeft;

    [SerializeField]
    string timeLeftFormat;

    [SerializeField]
    int currentTime = 180;

    public int CurrentTime
    {
        get { return currentTime; }
        set
        {
            if (value != currentTime)
            {
                currentTime = value;
                timeLeft.text = 
                    timeLeftFormat.FormatWith(currentTime);
            }
        }
    }

    Sequence countdownSequence;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    public void StartCountdown()
    {
        countdownSequence =
        DOTween.Sequence()
               .Append(timeBarGreen.DOFillAmount(0, gameCtrl.countdownDuration)
                                   .SetEase(Ease.Linear))
               .Join(DOTween.To(() => { return CurrentTime; },
                                (x) => { CurrentTime = x; },
                                0,
                                gameCtrl.countdownDuration)
                            .SetEase(Ease.Linear))
               .OnStart(() => { currentTime = gameCtrl.countdownDuration; })
               .OnComplete(() => { gameCtrl.Notify_CountdownFinished(); });
    }

    public void StopCountdown()
    {
        if(countdownSequence != null && countdownSequence.IsPlaying())
            countdownSequence.Kill();
    }

    #endregion

}

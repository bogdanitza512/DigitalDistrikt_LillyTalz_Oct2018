﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Quizz.Miscellaneous
{
    public enum InteractionState { Idle, Selected }

    public enum ValidityState { Uncertain, Right, Wrong };

    public struct SuperState
    {
        public InteractionState interactionState;

        public ValidityState validityState;

        public SuperState(InteractionState _variantState,
                          ValidityState _validityState)
        {
            interactionState = _variantState;
            validityState = _validityState;
        }
    }

    public struct StateTransitionSpecs
    {
        public float canvasGroupAlpha;

        public Color outlineColor;

        public Color textColor;

        public Color sidePanelColor;

        public float durationMultiplier;

        public int loops;

        public StateTransitionSpecs(float _canvasGroupAlpha,
                                    Color _highlightColor,
                                    Color _textColor,
                                    Color _sidePanelColor,
                                    float _durationMultiplier,
                                    int _loops)
        {
            canvasGroupAlpha = _canvasGroupAlpha;
            outlineColor = _highlightColor;
            textColor = _textColor;
            sidePanelColor = _sidePanelColor;
            durationMultiplier = _durationMultiplier;
            loops = _loops;
        }

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Sirenix.OdinInspector;
using BN512.Extensions;
using DG.Tweening;
using Quizz.Miscellaneous;
using System;

/// <summary>
/// 
/// </summary>
public class QuizzManager : SerializedMonoBehaviour {

    #region Nested Types

    #endregion

    #region Fields and Properties

    public KnowAndTreatGameController gameCtrl;

    public TMP_Text questionText;

    public float baseDuration = 1.0f;

    public List<AnswearVariant> answearVariants = new List<AnswearVariant>();

    public Dictionary<SuperState, StateTransitionSpecs> transitionDict = 
        new Dictionary<SuperState, StateTransitionSpecs>();

    Queue<int> lastAnsweredQuestions = new Queue<int>();

    public bool isLocked = false;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
        if (questionText == null)
        {
            AutoPopulateFields();
        }
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    [Button(ButtonSizes.Medium)]
    public void AutoPopulateFields()
    {
        foreach (Transform child in transform)
        {
            if (questionText == null)
            {
                questionText = child.GetComponent<TMP_Text>();
            }
            var ansVar = child.GetComponent<AnswearVariant>();
            if(ansVar != null)
            {
                answearVariants.Add(ansVar);
                //ansVar.questionManager = this;
            }
        }

        if(transitionDict == null)
        {
            transitionDict = new Dictionary<SuperState, StateTransitionSpecs>();
            transitionDict.Add(new SuperState(InteractionState.Idle, ValidityState.Right),
                               new StateTransitionSpecs(1, Color.white, Color.black, Color.gray, 1, 3));
            transitionDict.Add(new SuperState(InteractionState.Idle, ValidityState.Uncertain),
                               new StateTransitionSpecs(1, Color.white, Color.black, Color.gray, 1, 3));
            transitionDict.Add(new SuperState(InteractionState.Idle, ValidityState.Wrong),
                               new StateTransitionSpecs(1, Color.white, Color.black, Color.gray, 1, 3));
            transitionDict.Add(new SuperState(InteractionState.Selected, ValidityState.Right),
                               new StateTransitionSpecs(1, Color.white, Color.black, Color.gray, 1, 3));
            transitionDict.Add(new SuperState(InteractionState.Selected, ValidityState.Uncertain),
                               new StateTransitionSpecs(1, Color.white, Color.black, Color.gray, 1, 3));
            transitionDict.Add(new SuperState(InteractionState.Selected, ValidityState.Wrong),
                               new StateTransitionSpecs(1, Color.white, Color.black, Color.gray, 1, 3));
        }
    }

    [Button(ButtonSizes.Medium)]
    public void LoadRandomQuestion()
    {
        gameCtrl.currentQuestionIndex = 
            GetRandomValidQuestionIndex(gameCtrl.Keyword.Length - 1);

        questionText.text = 
            gameCtrl.CurrentQuestion.questionText;

        for (int i = 0; i < answearVariants.Count; i++)
        {
            if (i < gameCtrl.CurrentQuestion.answearVariants.Count)
            {

                answearVariants[i].Initialize(gameCtrl.CurrentQuestion.answearVariants[i]);
            }
            else
            {
                answearVariants[i].MakeInvisible();
            }
        }
    }

    int GetRandomValidQuestionIndex(int errorThreshold)
    {
        int index;
        bool isIndexValid = false;
        int currentError = 0;
        do
        {
            index = gameCtrl.config.QuizzQuestions.GetRandomIndex();

            if (!lastAnsweredQuestions.Contains(index))
            {
                isIndexValid = true;
            }

            currentError += isIndexValid ? 0 : 1;

            if (currentError >= errorThreshold * 2)
            {
                isIndexValid = true;
            }
        
        } while (isIndexValid == false);

        lastAnsweredQuestions.Enqueue(index);

        if (lastAnsweredQuestions.Count >= errorThreshold)
        {
            lastAnsweredQuestions.Dequeue();
        }

        return index;
    }

    public void OnSendAnswear_Click()
    {
        if (isLocked) return;
        bool overallValidity = true;
        var duration = baseDuration;

        foreach (var answearVariant in answearVariants)
        {
            overallValidity &= answearVariant.ResolveValidity();
            duration += overallValidity ? baseDuration * .25f : baseDuration * .50f;
        }

        DOTween.Sequence()
               .AppendInterval(duration)
               .OnStart(() =>
                {
                    if (overallValidity)
                    {
                        gameCtrl.Notify_ValidAnswear();
                    }
                })
               .OnComplete(() => { LoadRandomQuestion(); });
    }

    public void Reset()
    {
        foreach(var answearVariant in answearVariants)
        {
            answearVariant.Reset();
        }
        LoadRandomQuestion();
    }

    #endregion

}

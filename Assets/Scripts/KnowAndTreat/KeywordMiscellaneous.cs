﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Keyword.Miscellaneous
{
    public enum State { Correct, Hidden, Incorect, Winner, Loser}

    [System.Serializable]
    public struct StateSpecs
    {
        public int loops;

        public Color bigSpoonColor;
        public Color littleSpoonColor;
        public Color letterColor;

        public float durationMultiplier;


        public StateSpecs(int _loops,
                          Color _bigSpoonColor,
                          Color _smallSpoonColor,
                          Color _letterColor,
                          float _durationMultiplier)
        {
            
            bigSpoonColor = _bigSpoonColor;
            littleSpoonColor = _smallSpoonColor;
            letterColor = _letterColor;
            durationMultiplier = _durationMultiplier;
            loops = _loops;
        }

    }
}

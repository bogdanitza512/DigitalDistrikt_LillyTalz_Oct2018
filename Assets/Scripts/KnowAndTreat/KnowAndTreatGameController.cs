﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using ScreenMgr;
using TMPro;
using Game.ConfigMiscellaneous;
using DG.Tweening;
using Keyword.Miscellaneous;

/// <summary>
/// 
/// </summary>
public class KnowAndTreatGameController : SerializedMonoBehaviour
{

    #region Nested Types

    #endregion

    #region Fields and Properties

    public GameConfig config;

    public ScreenManager masterScreenManager;

    public BaseScreen currentScreen;

    public GameMode gameMode;

    public QuizzCountdownManager countdownManager;

    public ScreenManager miniGameScreenManager;

    public BaseScreen quizzScreen;

    public BaseScreen puzzleScreen;

    public QuizzManager quizzManager;

    public PuzzleManager puzzleManager;

    public KeywordManager keywordManager;

    public AvatarManager avatarManager;

    public int countdownDuration = 180;

    public int keywordIndex;

    public int currentQuestionIndex;
    public int score;

    public string Keyword
    {
        get
        {
            return config.Keywords[keywordIndex];
        }
            
    }

    public QuizzQuestionContent CurrentQuestion
    {
        get
        {
            return config.QuizzQuestions[currentQuestionIndex];
        }

    }


    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    public void StartGame()
    {
        keywordManager.LoadRandomValidWord();
        countdownManager.StartCountdown();
        avatarManager.StartRotatingMovement();
        quizzManager.LoadRandomQuestion();
    }

    public void EndGame()
    {
        countdownManager.StopCountdown();
        avatarManager.EliminateRemainingSports();
        DOTween.Sequence()
               .AppendInterval(keywordManager.baseTransitionDuration)
               .OnComplete(() =>
                {
                    keywordManager.TransitioAllLetterBoxesTo(State.Winner);
                    masterScreenManager.HideAll();
                });
        print("End of Game");
    }

    public void RestartGame()
    {
        gameMode.RestartScene();
    }

    public void Notify_CountdownFinished()
    {
        print("Notify_CountdownFinished");
        quizzManager.isLocked = true;

        miniGameScreenManager.ShowScreen(puzzleScreen);
        //puzzleManager.InitPuzzlePieces();
    }

    public void Notify_ValidAnswear()
    {
        score++;

        keywordManager.RevealRandomLetter();
        avatarManager.EliminateRandomSpots();

        if (score == Keyword.Length)
        {
            EndGame();
        }
    }

    public void Notify_LetterMatch(LetterBox letter)
    {
        score++;

        keywordManager.RevealLetter(letter);

        DOTween.Sequence()
               .AppendInterval(keywordManager.baseTransitionDuration)
               .OnStart(() => { avatarManager.EliminateRandomSpots(); })
               .OnComplete(() =>
               {
                   if (score == Keyword.Length)
                   {
                       EndGame();
                   }
               });

    }

    #endregion

}

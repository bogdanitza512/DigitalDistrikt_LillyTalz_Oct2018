﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using BN512.Extensions;

/// <summary>
/// 
/// </summary>
public class AvatarManager : MonoBehaviour {

    #region Nested Types



    #endregion

    #region Fields and Properties

    public KnowAndTreatGameController gameCtrl;

    public GameObject parent;
    public float baseDuration = 2.5f;

    public List<GameObject> spotsList;

    public int initialSpotsCount;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
        parent.SetActive(false);
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    public void StopRotating()
    {
        parent.SetActive(false);
    }

    public void EliminateRemainingSports()
    {
        foreach(var spot in spotsList)
        {
            spot.SetActive(false);
        }
    }

	public void StartRotatingMovement()
    {
        parent.SetActive(true);

        initialSpotsCount = spotsList.Count;

        parent.transform.DORotate(parent.transform.eulerAngles.WithY(360),
                                  baseDuration)
                        .SetEase(Ease.Linear)
                        .SetLoops(-1, LoopType.Incremental);
    }

    public void EliminateRandomSpots()
    {
        
        int spotNb = (initialSpotsCount / gameCtrl.Keyword.Length);
        if (spotsList.Count >= spotNb)
        {
            while (spotNb != 0)
            {
                int index = Random.Range(0, spotsList.Count);
                spotsList[index].SetActive(false);
                spotsList.RemoveAt(index);
                spotNb--;
            }
        } else if(spotsList.Count != 0)
        {
            foreach (var spots in spotsList)
            {
                spots.SetActive(false);
            }
            spotsList.Clear();
        }
    }

    #endregion

}

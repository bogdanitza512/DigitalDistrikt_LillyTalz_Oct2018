﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BN512.Extensions;
using Sirenix.OdinInspector;
using System.Linq;
using Keyword.Miscellaneous;

/// <summary>
/// 
/// </summary>
public class PuzzleManager : MonoBehaviour
{

    #region Nested Types



    #endregion

    #region Fields and Properties

    public KnowAndTreatGameController gameCtrl;

    public float snapThreshold = .5f;

    public Transform puzzleBoxParent;

    public Transform scrambledPiecesParent;

    public List<PuzzlePiece> puzzlePieces;

    public List<string> alphabet =
        new List<string>()
    { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
        "N", "O", "P", "Q", "R", "S", "T", "U", "V","W", "x", "Y", "Z"};
    public Canvas masterCanvas;
    public float baseTweenDuration;



    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {

    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {

    }

    #endregion

    #region Methods

    [Button(ButtonSizes.Medium)]
    public void AutoPopulateFields()
    {
        if (scrambledPiecesParent == null)
            scrambledPiecesParent = transform;
        foreach (Transform child in puzzleBoxParent)
        {
            var puzzlePiece = child.GetComponent<PuzzlePiece>();
            if (puzzlePiece != null)
                puzzlePieces.Add(puzzlePiece);
        }
    }

    public List<PuzzlePiece>  UnAssignedPuzzlePieces
    {
        get
        {
            return puzzlePieces
                .Where((piece) => !piece.isAssigned)
                .ToList();
        }
    }

    public void InitPuzzlePieces()
    {

        if(puzzlePieces.IsEmpty())
        {
            AutoPopulateFields();
        }

        var hiddenLetterBoxes = 
            gameCtrl.keywordManager
                    .letterBoxes
                    .Where((box) => !box.isInvisible)
                    .ToList();

        foreach (var letterBox in hiddenLetterBoxes)
        {
            var puzzlePiece = UnAssignedPuzzlePieces.GetRandomItem();

            if (letterBox.currentState == State.Hidden)
            {
                puzzlePiece.Assign(letterBox);
                alphabet.Remove(letterBox.Content);
            }
            else
            {
                string letter = alphabet.GetRandomItem();
                puzzlePiece.Assign(letter);
                alphabet.Remove(letter);
            }

            puzzlePiece.Init(scrambledPiecesParent);
        }

        puzzleBoxParent.gameObject.SetActive(false);
    }
	

    #endregion

}

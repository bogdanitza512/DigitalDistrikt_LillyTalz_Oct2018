﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEditor;
using Keyword.Miscellaneous;

/// <summary>
/// 
/// </summary>
public class LetterBox : MonoBehaviour {

    #region Nested Types

    #endregion

    #region Fields and Properties

    public KeywordManager keywordManager;

    public State currentState = State.Hidden;

    public CanvasGroup canvasGroup;

    public Image bigSpoon;

    public Image littleSpoon;

    public TMP_Text letter;
    public bool isInvisible = false;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
        if(canvasGroup == null || bigSpoon == null || littleSpoon == null || letter == null)
        {
            AutoPopulateFields();
        }
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    [Button(ButtonSizes.Medium)]
    public void AutoPopulateFields()
    {
        if(canvasGroup == null)
            canvasGroup = GetComponent<CanvasGroup>();

        foreach(Transform child in transform)
        {
            if(bigSpoon == null)
            {
                var image = child.GetComponent<Image>();
                if (image != littleSpoon)
                    bigSpoon = image;
            }
            if (littleSpoon == null)
            {
                var image = child.GetComponent<Image>();
                if (image != bigSpoon)
                    littleSpoon = image;
            }
            if (letter == null)
            {
                letter = child.GetComponent<TMP_Text>();
            }
        }
    }

    public string Content
    {
        get
        {
            return letter.text;
        }
        set
        {
            letter.text = value;
        }
    }

    public void Initialize(char character,State initialState)
    {
        isInvisible = false;
        canvasGroup.alpha = 1;
        letter.text = character.ToString();
        TransitionTo(initialState);
    }


    public void MakeInvisible()
    {
        isInvisible = true;
        canvasGroup.alpha = 0;
        letter.text = "";
    }

    public void TransitionTo(State state, bool modifyCurrentState = true)
    {
        if (Application.isPlaying)
        {
            TransitionTo_DOTween(keywordManager.transitionDict[state]);
        }
        else
        {
            TransitionTo_NoLerp(keywordManager.transitionDict[state]);
        }

        if (modifyCurrentState)
        {
            currentState = state;
        }
    }

    Sequence transitionSeq;

    void TransitionTo_DOTween(StateSpecs specs)
    {
        var duration = keywordManager.baseTransitionDuration * specs.durationMultiplier;

        /*
        if ((transitionSeq != null) && (!transitionSeq.IsPlaying()))
        {
            transitionSeq.Complete();
        }

        transitionSeq =
            DOTween.Sequence()
                   .Append(bigSpoon.DOColor(specs.bigSpoonColor, duration))
                   .Join(littleSpoon.DOColor(specs.littleSpoonColor, duration))
                   .Join(letter.DOColor(specs.letterColor, duration))
                   .SetLoops(specs.loops)
                   .Play();
        */

        //CheckIfTweeningAndComplete(bigSpoon);

        CheckIfTweeningAndComplete(bigSpoon);
        bigSpoon.DOColor(specs.bigSpoonColor, duration)
                .SetLoops(specs.loops);
        
        CheckIfTweeningAndComplete(littleSpoon);
        littleSpoon.DOColor(specs.littleSpoonColor, duration)
                   .SetLoops(specs.loops);
        
        CheckIfTweeningAndComplete(letter);
        letter.DOColor(specs.letterColor, duration)
              .SetLoops(specs.loops);
        
    }

    private void CheckIfTweeningAndComplete(object target)
    {
        if (DOTween.IsTweening(target))
        {
            DOTween.Complete(target);
        }
    }


    void TransitionTo_NoLerp(StateSpecs specs)
    {
        CheckIfTweeningAndComplete(bigSpoon);
        bigSpoon.color = specs.bigSpoonColor;

        CheckIfTweeningAndComplete(littleSpoon);
        littleSpoon.color = specs.littleSpoonColor;

        CheckIfTweeningAndComplete(letter);
        letter.color = specs.letterColor;
    }

    public void Reset()
    {
        letter.text = " ";
        currentState = State.Hidden;
        TransitionTo_NoLerp(keywordManager.transitionDict[currentState]);

    }

    #endregion

}

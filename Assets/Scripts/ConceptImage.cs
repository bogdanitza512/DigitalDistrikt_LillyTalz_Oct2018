﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BN512.Extensions;
using Sirenix.OdinInspector;

/// <summary>
/// 
/// </summary>
[ExecuteInEditMode]
public class ConceptImage : MonoBehaviour {

    #region Nested Types



    #endregion

    #region Fields and Properties

    public Image conceptImage;

    [Range(0,1)]
    public float conceptOpacity = 0;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {

        if (conceptImage == null)
            AutoPopulate();
        
        conceptImage.color = Color.white.WithAlpha(0);
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
        conceptImage.color = conceptImage.color.WithAlpha(conceptOpacity);
    }

    #endregion

    #region Methods

    [Button(ButtonSizes.Medium)]
    void AutoPopulate()
    {
        conceptImage = GetComponent<Image>();
        
    }
	

    #endregion

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using BN512.Extensions;

public class PulsatingSize : MonoBehaviour {
    
    public float pulseStrenght = 1.25f;
    public float pulseLenght = .5f;

	// Use this for initialization
	void Start () {
        
        transform.DOScale(transform.localScale * pulseStrenght,pulseLenght)
                 .SetLoops(-1,LoopType.Yoyo);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

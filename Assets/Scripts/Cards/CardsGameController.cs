﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BN512.Extensions;
using Sirenix.OdinInspector;
using System;
using ScreenMgr;


/// <summary>
/// 
/// </summary>
public class CardsGameController : SerializedMonoBehaviour
{

    #region Nested Types



    #endregion

    #region Fields and Properties

    public GameConfig config;

    public GameMode gameMode;

    public ScreenManager screenManager;

    public QuestionCardManager questionCardManager;

    public AnswearCardManager answearCardManager;

    public CardsCountdownManager countdownManager;

    public RevealManager revealManager;

    public Dictionary<QuestionCard, AnswearCard> QADict =
        new Dictionary<QuestionCard, AnswearCard>();

    public int countdownDuration = 15;
    public int score;
    public int qaCount = 0;
    public int QACount
    {
        get
        {
            if(qaCount == 0)
            {
                var qCount = questionCardManager.cards.Count;
                var aCount = answearCardManager.cards.Count;
                qaCount = aCount;
            }
            return qaCount;
        }
    }

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    [Button(ButtonSizes.Medium)]
    public void AutoPopulateFields()
    {
        foreach(Transform child in transform)
        {
            var qCardManager = child.GetComponent<QuestionCardManager>();
            if(qCardManager != null)
            {
                questionCardManager = qCardManager;
            }

            var aCardManager = child.GetComponent<AnswearCardManager>();
            if (aCardManager != null)
            {
                answearCardManager = aCardManager;
            }
        
        }
    }

	public void StartGame()
    {
        LoadQuestionsAndAnswears();
        questionCardManager.ActivateTopMostCard();
        revealManager.SetRandomRevealImage();
    }

    private void LoadQuestionsAndAnswears()
    {
        config.CardQA.Shuffle();

        var qaArr = config.CardQA.GetRange(0, QACount);
        foreach (var qa in qaArr)
        {
            // print("[QUESTION]: {0}".FormatWith(qa.question));
            // print("[ANSWEAR]: {0}".FormatWith(qa.answear));

            QADict.Add(questionCardManager.AssignToRandomCard(qa.question),
                                      answearCardManager.AssignToRandomCard(qa.answear));
        }
    }

    public void Notify_CountdownFinished()
    {
        questionCardManager.MoveQuestionToEndOfDeck();
        questionCardManager.ActivateTopMostCard();
        countdownManager.Hide();
    }

    public void ComputeValidity()
    {
        if (questionCardManager.currentQuestion.CurrentState !=
            Cards.Miscellaneous.QuestionState.FaceDown)
        {
            if (QADict[questionCardManager.currentQuestion] ==
                       answearCardManager.currentAnswear)
            {
                Notify_CorrectAnswear();
            }
            else
            {
                Notify_IncorrectAnswear();
            }
        }
    }

    public void Notify_QuestionCardFacedUp()
    {
        countdownManager.Reveal();
        countdownManager.StartCountdown();
    }

    public void Notify_CorrectAnswear()
    {
        print("Correct Answear");
        countdownManager.StopCountdown();
        countdownManager.Hide();
        questionCardManager.HideCurrentQuestionCard();
        answearCardManager.HideCurrentAnswearCard();
        score++;
        if(score == qaCount)
        {
            revealManager.RefreshOpacity();
            EndGame();
        }
        else
        {
            revealManager.RefreshOpacity();
            questionCardManager.ActivateTopMostCard();
        }


    }

    public void Notify_IncorrectAnswear()
    {
        answearCardManager.currentAnswear.ShakeCard();
    }

    public void Notify_QuestionCardFacedDown()
    {
        countdownManager.Hide();
        countdownManager.StopCountdown();
    }

    public void EndGame()
    {
        print("End of the ride!");
        screenManager.HideAll();
    }

    public void RestartGame()
    {
        gameMode.RestartScene();
    }

    #endregion

}

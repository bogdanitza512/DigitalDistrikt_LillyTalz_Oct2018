﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BN512.Extensions;
using DG.Tweening;

/// <summary>
/// 
/// </summary>
public class RevealManager : MonoBehaviour {

    #region Nested Types



    #endregion

    #region Fields and Properties

    public CardsGameController gameCtrl;

    public Image revealImage;

    public List<Sprite> images = new List<Sprite>();

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

	public void SetRandomRevealImage()
    {
        revealImage.sprite = images.GetRandomItem();
    }

    public void RefreshOpacity()
    {
        revealImage.DOFade((1.0f / (float)gameCtrl.qaCount) * (float)gameCtrl.score, 1.0f);
        print((1.0f / (float)gameCtrl.qaCount) * (float)gameCtrl.score);
    }

    #endregion

}

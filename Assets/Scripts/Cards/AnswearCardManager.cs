﻿using System;
using System.Collections;
using System.Collections.Generic;
using BN512.Extensions;
using UnityEngine;
using Sirenix.OdinInspector;
using Cards.Miscellaneous;
using System.Linq;

/// <summary>
/// 
/// </summary>
public class AnswearCardManager : SerializedMonoBehaviour {

    #region Nested Types



    #endregion

    #region Fields and Properties

    public CardsGameController gameCtrl;

    public Dictionary<AnswearState, AnswearTransitionSpecs> transitionDict =
        new Dictionary<AnswearState, AnswearTransitionSpecs>
    {
        { AnswearState.FaceDown, new AnswearTransitionSpecs() },
        { AnswearState.FaceUp, new AnswearTransitionSpecs() },
        { AnswearState.Hidden, new AnswearTransitionSpecs() }
    };

    public List<AnswearCard> cards =  new List<AnswearCard>();
    
    public float baseDuration = 1.0f;

    public Transform hiddenTransform;

    public AnswearCard currentAnswear;
    public float shakeDuration;
    public float shakeStrength;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    [Button(ButtonSizes.Medium)]
    public void AutoPopulateFields()
    {
        cards.Clear();

        foreach (Transform child in transform)
        {
            var aCard = child.GetComponent<AnswearCard>();
            if (aCard != null)
            {
                cards.Add(aCard);
                aCard.cardManager = this;
                aCard.AutoPopulateFields();
            }
        }

        // cards.Reverse();
    
    }

    public AnswearCard AssignToRandomCard(string answear)
    {
        var answearCard = cards.Where((obj) => !gameCtrl.QADict.ContainsValue(obj))
                               .ToList()
                               .GetRandomItem();

        answearCard.Assign(answear);

        return answearCard;
    }

    public void ComputeValidity(AnswearCard answearCard)
    {
        currentAnswear = answearCard;
        gameCtrl.ComputeValidity();
    }

    internal void HideCurrentAnswearCard()
    {
        currentAnswear.CurrentState = AnswearState.Hidden;
    }

    #endregion

}

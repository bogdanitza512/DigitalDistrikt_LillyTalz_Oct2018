﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Cards.Miscellaneous;
using System;
using BN512.Extensions;
using DG.Tweening;
using System.Linq;

/// <summary>
/// 
/// </summary>
public class QuestionCardManager : SerializedMonoBehaviour {

    #region Nested Types



    #endregion

    #region Fields and Properties

    public CardsGameController gameCtrl;

    public QuestionCard currentQuestion;

    public float baseDuration = 1;

    public Dictionary<QuestionState, QuestionTransitionSpecs> transitionDict =
        new Dictionary<QuestionState, QuestionTransitionSpecs>()
    {
        {QuestionState.FaceDown,new QuestionTransitionSpecs()},
        {QuestionState.FaceUp,new QuestionTransitionSpecs()}
    };

    public List<QuestionCard> cards;

    public Transform hiddenTransform;
    public Transform auxTransform;
    public Transform focusTransform;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    [Button(ButtonSizes.Medium)]
    public void AutoPopulateFields()
    {
        cards.Clear();

        foreach(Transform child in transform)
        {
            var qcard = child.GetComponent<QuestionCard>();
            if(qcard != null)
            {
                cards.Add(qcard);
                qcard.cardManager = this;
                qcard.AutoPopulateFields();
            }
        }

        cards.Reverse();
    }

    public QuestionCard AssignToRandomCard(string question)
    {
        var card = cards.Where((obj) => !gameCtrl.QADict.ContainsKey(obj))
                        .ToList()
                        .GetRandomItem();

        card.Assign(question);

        return card;
    }

    public void HideCurrentQuestionCard()
    {
        var go = currentQuestion.gameObject;

        currentQuestion.transformProxy
                       .DOMove(hiddenTransform.position,
                               baseDuration)
                       .OnComplete(() =>
                       {
                            Destroy(go);
                       });

        cards.Remove(currentQuestion);
       
    }

    public void ActivateTopMostCard()
    {
        currentQuestion = cards[0];
        currentQuestion.IsActive = true;
        currentQuestion.CurrentState = 
            QuestionState.FaceDown;
    }

    public void MoveQuestionToEndOfDeck()
    {
        currentQuestion = cards[0];
        cards.RemoveAt(0);
        currentQuestion.IsActive = false;

        DOTween.Sequence()
               .Append(currentQuestion.transformProxy
                       .DOMove(auxTransform.position,
                               baseDuration / 2))
               .AppendCallback(currentQuestion.transform.SetAsFirstSibling)
               .Append(currentQuestion.transformProxy
                       .DOMove(currentQuestion.initialPosition,
                               baseDuration / 2))
               .Play();

        currentQuestion.CurrentState = 
            QuestionState.FaceDown;
        cards.Add(currentQuestion);
        currentQuestion = cards[0];
    }

    #endregion

}

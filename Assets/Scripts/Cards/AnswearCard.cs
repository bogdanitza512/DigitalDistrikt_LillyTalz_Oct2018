﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Cards.Miscellaneous;
using DG.Tweening;
using BN512.Extensions;
using UnityEngine.EventSystems;

/// <summary>
/// 
/// </summary>
public class AnswearCard : MonoBehaviour {

    #region Nested Types



    #endregion

    #region Fields and Properties

    public Image faceUp;

    public Image faceDown;

    public TMP_Text answearText;

    public AnswearCardManager cardManager;

    public Vector3 initialPosition;

    public Transform transformProxy;

    [SerializeField]
    string currentAnswearText;

    public string AnswearText
    {
        get
        {
            return answearText.text;
        }

        set
        {
            currentAnswearText = value;
            answearText.text = value;
        }
    }

    [SerializeField]
    AnswearState currentState;

    public AnswearState CurrentState
    {
        get
        {
            return currentState;
        }

        set
        {
            if (currentState != value)
            {
                currentState = value;

                if (Application.isPlaying)
                {
                    TransitionTo_WithTween(cardManager.transitionDict[currentState]);
                }
                else
                {
                    TransitionTo_WithSnap(cardManager.transitionDict[currentState]);
                }
            }
        }
    }

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
        
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    [Button(ButtonSizes.Medium)]
    public void AutoPopulateFields()
    {
        
        foreach (Transform child in transform)
        {
            if (child.name == "[Transform_Proxy]")
            {
                transformProxy = child;
                initialPosition = transformProxy.position;
            }
        }

        foreach (Transform child in transformProxy)
        {
            if (child.name == "Image_Card_FaceUp")
            {
                faceUp = child.GetComponent<Image>();
            }

            if (child.name == "Image_Card_FaceDown")
            {
                faceDown = child.GetComponent<Image>();
            }

            if (child.name == "Text_Answear")
            {
                answearText = child.GetComponent<TMP_Text>();
            }
        }
    }

    public void Assign(string _questionText)
    {
        AnswearText = _questionText;

        CurrentState = AnswearState.FaceUp;
    }

    private void TransitionTo_WithTween(AnswearTransitionSpecs specs)
    {
        var duration = cardManager.baseDuration * specs.durationMultiplier;

        CheckIfTweeningAndComplete(faceUp);
        faceUp.DOFade(specs.faceUpAlpha, duration);

        CheckIfTweeningAndComplete(faceDown);
        faceDown.DOFade(specs.faceDownAlpha, duration);

        CheckIfTweeningAndComplete(answearText);
        answearText.DOFade(specs.answearTextAlpha, duration);

        CheckIfTweeningAndComplete(transformProxy);
        switch(currentState)
        {
            case AnswearState.Hidden:
                transformProxy.DOMove(cardManager.hiddenTransform.position, duration);
                break;

            default:
                transformProxy.DOMove(initialPosition, duration);
                break;
        }
    }

    private void TransitionTo_WithSnap(AnswearTransitionSpecs specs)
    {

        CheckIfTweeningAndComplete(faceUp);
        faceUp.color =
            faceUp.color.WithAlpha(specs.faceUpAlpha);

        CheckIfTweeningAndComplete(faceDown);
        faceDown.color =
            faceDown.color.WithAlpha(specs.faceUpAlpha);

        CheckIfTweeningAndComplete(answearText);
        answearText.color = 
            answearText.color.WithAlpha(specs.faceUpAlpha);

        CheckIfTweeningAndComplete(transformProxy);
        switch (currentState)
        {
            case AnswearState.Hidden:
                transformProxy.position = cardManager.hiddenTransform.position;
                break;

            default:
                transformProxy.position = initialPosition;
                break;
        }
    }

    private void CheckIfTweeningAndComplete(object target)
    {
        if (DOTween.IsTweening(target))
        {
            DOTween.Complete(target);
        }
    }

    public void OnAnswearCard_Click()
    {
        if(CurrentState == AnswearState.FaceUp)
        {
            cardManager.ComputeValidity(this);
            //CurrentState = AnswearState.FaceDown;
        }

    }
    public void ShakeCard()
    {
        transformProxy
            .DOShakePosition(cardManager.shakeDuration, 
                             new Vector3().With(cardManager.shakeStrength,
                                                cardManager.shakeStrength));
        print("Shaking..");
    }

    #endregion

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cards.Miscellaneous
{
    public enum QuestionState { FaceDown, FaceUp }

    public struct QuestionTransitionSpecs
    {
        
        public float faceUpAlpha;

        public float faceDownAlpha;

        public float questionTextAlpha;

        public float durationMultiplier;

        public QuestionTransitionSpecs(float _faceUpAlpha = 1,
                                       float _faceDownAlpha = 1,
                                       float _textAlpha = 1,
                                       float _durationMultiplier = 1)
        {
            faceUpAlpha = _faceUpAlpha;
            faceDownAlpha = _faceDownAlpha;
            questionTextAlpha = _textAlpha;
            durationMultiplier = _durationMultiplier;
        }

    }

    public enum AnswearState { FaceDown, FaceUp, Hidden }

    public struct AnswearTransitionSpecs
    {
        
        public float faceUpAlpha;

        public float faceDownAlpha;

        public float answearTextAlpha;

        public float durationMultiplier;

        public AnswearTransitionSpecs(float _faceUpAlpha = 1,
                                      float _faceDownAlpha = 1,
                                      float _textAlpha = 1,
                                      float _durationMultiplier = 1)
        {
            faceUpAlpha = _faceUpAlpha;
            faceDownAlpha = _faceDownAlpha;
            answearTextAlpha = _textAlpha;
            durationMultiplier = _durationMultiplier;
        }

    }

}

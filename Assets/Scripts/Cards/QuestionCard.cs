﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;
using TMPro;
using Cards.Miscellaneous;
using DG.Tweening;
using BN512.Extensions;
using System;
using UnityEngine.EventSystems;

/// <summary>
/// 
/// </summary>
public class QuestionCard : MonoBehaviour, IPointerClickHandler {

    #region Nested Types



    #endregion

    #region Fields and Properties

    public QuestionCardManager cardManager;

    public Image faceUp;

    public Image faceDown;

    public TMP_Text questionText;

    public Image inactive;

    public Transform transformProxy;

    [SerializeField]
    string currentQuestionText;

    public string QuestionText
    {
        get
        {
            return questionText.text;
        }

        set
        {
            currentQuestionText = value;
            questionText.text = value;
        }
    }



    public Vector3 initialPosition;

    [SerializeField]
    QuestionState currentState;

    public QuestionState CurrentState
    {
        get
        {
            return currentState;
        }

        set
        {
            if (currentState != value)
            {
                currentState = value;

                if (Application.isPlaying)
                {
                    TransitionTo_WithTween(cardManager.transitionDict[currentState]);
                }
                else
                {
                    TransitionTo_WithSnap(cardManager.transitionDict[currentState]);
                }
            }
        }
    }

    [SerializeField]
    bool isActive = true;

    public bool IsActive
    {
        get
        {
            return isActive;
        }

        set
        {
            if (isActive != value)
            {
                isActive = value;
                var alpha = isActive ? 0 : .5f;

                if (Application.isPlaying == true)
                {
                    inactive.DOFade(alpha,cardManager.baseDuration);
                }
                else 
                {
                    inactive.color = inactive.color.WithAlpha(alpha);
                }
            }
        }
    }



    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    [Button(ButtonSizes.Medium)]
	public void AutoPopulateFields()
    {
        
        foreach (Transform child in transform)
        {
            if (child.name == "[Transform_Proxy]")
            {
                transformProxy = child;
                initialPosition = transformProxy.position;
            }
        }

        foreach(Transform child in transformProxy)
        {
            if(child.name == "Image_Card_FaceUp")
            {
                faceUp = child.GetComponent<Image>();
            }

            if (child.name == "Image_Card_FaceDown")
            {
                faceDown = child.GetComponent<Image>();
            }

            if (child.name == "Text_Question")
            {
                questionText = child.GetComponent<TMP_Text>();
            }

            if (child.name == "Image_Card_Inactive")
            {
                inactive = child.GetComponent<Image>();
            }

        }
    }

    public void Assign(string _questionText)
    {
        QuestionText = _questionText;
        CurrentState = QuestionState.FaceDown;
        IsActive = false;
    }

    public void TransitionTo_WithTween(QuestionTransitionSpecs specs)
    {
        
        var duration = cardManager.baseDuration * specs.durationMultiplier;

        CheckIfTweeningAndComplete(faceUp);
        faceUp.DOFade(specs.faceUpAlpha, duration);

        CheckIfTweeningAndComplete(faceDown);
        faceDown.DOFade(specs.faceDownAlpha, duration);

        CheckIfTweeningAndComplete(questionText);
        questionText.DOFade(specs.questionTextAlpha, duration);

    }

    public void TransitionTo_WithSnap(QuestionTransitionSpecs specs)
    {

        CheckIfTweeningAndComplete(faceUp);
        faceUp.color = 
            faceUp.color.WithAlpha(specs.faceUpAlpha);

        CheckIfTweeningAndComplete(faceDown);
        faceDown.color = 
            faceDown.color.WithAlpha(specs.faceUpAlpha);

        CheckIfTweeningAndComplete(questionText);
        questionText.color = 
            questionText.color.WithAlpha(specs.faceUpAlpha);
        
    }

    private void CheckIfTweeningAndComplete(object target)
    {
        if (DOTween.IsTweening(target))
        {
            DOTween.Complete(target);
        }
    }

    void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
    {
        if (IsActive == true)
        {
            switch(CurrentState)
            {
                case QuestionState.FaceDown:
                    CurrentState = QuestionState.FaceUp;
                    cardManager.gameCtrl.Notify_QuestionCardFacedUp();
                    transformProxy.DOMove(cardManager.focusTransform.position, .5f);
                    break;
                case QuestionState.FaceUp:
                    CurrentState = QuestionState.FaceDown;
                    cardManager.gameCtrl.Notify_QuestionCardFacedDown();
                    transformProxy.DOMove(initialPosition, .5f);
                    break;
            }
        }
    }

    #endregion

}

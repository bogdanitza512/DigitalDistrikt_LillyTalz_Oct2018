﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Treatment.Reward.Miscellaneous
{

    public enum State { Locked, Unlocked }

    public struct StateTransitionSpecs
    {
        public float blueFillAlpha;

        public float greenFillAlpha;

        public float durationMultiplier;

        public int loops;

        public StateTransitionSpecs(float _blueFillAlpha,
                                    float _greenFillAlpha,
                                    float _durationMultiplier,
                                    int _loops)
        {
            blueFillAlpha = _blueFillAlpha;
            greenFillAlpha = _greenFillAlpha;

            durationMultiplier = _durationMultiplier;
            loops = _loops;
        }

    }
}

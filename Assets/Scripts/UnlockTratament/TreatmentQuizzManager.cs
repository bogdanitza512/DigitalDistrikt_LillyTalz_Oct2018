﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Sirenix.OdinInspector;
using BN512.Extensions;
using DG.Tweening;
using Treatment.Quizz.Miscellaneous;
using System;

/// <summary>
/// 
/// </summary>
public class TreatmentQuizzManager : SerializedMonoBehaviour {

    #region Nested Types

    #endregion

    #region Fields and Properties

    public TreatmentGameController gameCtrl;

    public TMP_Text questionText;

    public float baseDuration = 1.0f;

    public List<TreatmentAnswearVariant> answearVariants = 
        new List<TreatmentAnswearVariant>();

    public Dictionary<SuperState, StateTransitionSpecs> transitionDict = 
        new Dictionary<SuperState, StateTransitionSpecs>();
    
    public bool isLocked = false;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
        if (questionText == null)
        {
            AutoPopulateFields();
        }
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    [Button(ButtonSizes.Medium)]
    public void AutoPopulateFields()
    {
        answearVariants.Clear();
        foreach (Transform child in transform)
        {
            if (questionText == null)
            {
                questionText = child.GetComponent<TMP_Text>();
            }
            var ansVar = child.GetComponent<TreatmentAnswearVariant>();
            if(ansVar != null)
            {
                answearVariants.Add(ansVar);
                ansVar.quizzManager = this;
            }
        }
    }

    public void LoadCurrentQuestion()
    {
        questionText.text = 
            gameCtrl.CurrentQuestion.questionText;

        for (int i = 0; i < answearVariants.Count; i++)
        {
            if (i < gameCtrl.CurrentQuestion.answearVariants.Count)
            {

                answearVariants[i].Initialize(gameCtrl.CurrentQuestion.answearVariants[i]);
            }
            else
            {
                answearVariants[i].MakeInvisible();
            }
        }
    }

    public void OnSendAnswear_Click()
    {
        if (isLocked) return;
        bool overallValidity = true;
        var duration = baseDuration;

        foreach (var answearVariant in answearVariants)
        {
            overallValidity &= answearVariant.ResolveValidity();
            duration += overallValidity ? baseDuration * .5f : baseDuration;
        }
        //print(duration);
        DOTween.Sequence()
               .AppendInterval(duration)
               .OnStart(() => gameCtrl.Notify_AnswearComputed(overallValidity))
               .OnComplete(() => gameCtrl.Notify_QuestionAnsweard());
    }

    #endregion

}

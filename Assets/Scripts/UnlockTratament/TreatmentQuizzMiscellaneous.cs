﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Treatment.Quizz.Miscellaneous
{

    public enum InteractionState { Idle, Selected }

    public enum ValidityState { Uncertain, Right, Wrong };

    public struct SuperState
    {
        public InteractionState interactionState;

        public ValidityState validityState;

        public SuperState(InteractionState _variantState,
                          ValidityState _validityState)
        {
            interactionState = _variantState;
            validityState = _validityState;
        }
    }

    public struct StateTransitionSpecs
    {
        public float canvasGroupAlpha;

        public Color fillColor;

        public Color outlineColor;

        public Color textColor;

        public float durationMultiplier;

        public int loops;

        public StateTransitionSpecs(float _canvasGroupAlpha,
                                    Color _fillColor,
                                    Color _outlineColor,
                                    Color _textColor,
                                    float _durationMultiplier,
                                    int _loops)
        {
            canvasGroupAlpha = _canvasGroupAlpha;
            fillColor = _fillColor;
            outlineColor = _outlineColor;
            textColor = _textColor;

            durationMultiplier = _durationMultiplier;
            loops = _loops;
        }

    }
}

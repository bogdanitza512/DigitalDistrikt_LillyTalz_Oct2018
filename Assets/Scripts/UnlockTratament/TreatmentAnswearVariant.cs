﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEditor;
using BN512.Extensions;
using Treatment.Quizz.Miscellaneous;
using Game.ConfigMiscellaneous;

/// <summary>
/// 
/// </summary>
public class TreatmentAnswearVariant : MonoBehaviour {

    #region Nested Types

    #endregion

    #region Fields and Properties

    public TreatmentQuizzManager quizzManager;

    public CanvasGroup canvasGroup;

    public Image outline;

    public Image fill;

    public TMP_Text answearText;

    public ValidityState targetValidityState =
        ValidityState.Uncertain;

    public InteractionState currentInteractionState =
        InteractionState.Idle;

    public ValidityState currentValidityState =
        ValidityState.Uncertain;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
        if(answearText == null ||
           outline == null)
        {
            AutoPopulateFields();
        }
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    [Button(ButtonSizes.Medium)]
    public void AutoPopulateFields()
    {
        canvasGroup = GetComponent<CanvasGroup>();

        foreach (Transform child in transform)
        {

            if (child.name == "Text_Answear")
            {
                answearText = child.GetComponent<TMP_Text>();
            }

            if (child.name == "Image_Outline")
            {
                outline = child.GetComponent<Image>();
            }

            if (child.name == "Image_Fill")
            {
                fill = child.GetComponent<Image>();
            }

        }
    }

    public void OnAnswearVariant_Click()
    {
        if (quizzManager.isLocked) return;
        if (currentValidityState == ValidityState.Uncertain)
        {
            if (currentInteractionState == InteractionState.Idle)
            {
                TransitionTo(new SuperState(InteractionState.Selected, currentValidityState));
            }
            else
            {
                TransitionTo(new SuperState(InteractionState.Idle, currentValidityState));
            }
        }
    }

    public void Initialize(AnswearVariantContent content)
    {
        answearText.text = content.answearVariantText;


        TransitionTo(new SuperState(InteractionState.Idle, ValidityState.Uncertain));

        targetValidityState = content.isCorrect ?
                                     ValidityState.Right :
                                     ValidityState.Wrong;
    }

    public void MakeInvisible()
    {
        TransitionTo_NoLerp(new StateTransitionSpecs(0.0f,
                                                     Color.white.WithAlpha(0),
                                                     Color.white.WithAlpha(0),
                                                     Color.white.WithAlpha(0),
                                                     1,
                                                     0));
        answearText.text = "";
        targetValidityState = ValidityState.Uncertain;
    }

    public void TransitionTo(SuperState superState)
    {
        if (Application.isPlaying)
        {
            TransitionTo_DOTween(quizzManager.transitionDict[superState]);
        }
        else
        {
            TransitionTo_NoLerp(quizzManager.transitionDict[superState]);
        }

        currentValidityState = superState.validityState;
        currentInteractionState = superState.interactionState;
    }

    private void TransitionTo_DOTween(StateTransitionSpecs specs)
    {
        var duration = quizzManager.baseDuration * specs.durationMultiplier;

        CheckIfTweeningAndComplete(canvasGroup);
        canvasGroup.DOFade(specs.canvasGroupAlpha, duration)
                   .SetLoops(specs.loops);

        CheckIfTweeningAndComplete(fill);
        fill.DOColor(specs.fillColor, duration)
                 .SetLoops(specs.loops);

        CheckIfTweeningAndComplete(outline);
        outline.DOColor(specs.outlineColor, duration)
                 .SetLoops(specs.loops);

        CheckIfTweeningAndComplete(answearText);
        answearText.DOColor(specs.textColor,duration)
                   .SetLoops(specs.loops);
    }

    private void TransitionTo_NoLerp(StateTransitionSpecs specs)
    {
        CheckIfTweeningAndComplete(canvasGroup);
        canvasGroup.alpha = specs.canvasGroupAlpha;

        CheckIfTweeningAndComplete(fill);
        fill.color = specs.fillColor;

        CheckIfTweeningAndComplete(outline);
        outline.color = specs.outlineColor;

        CheckIfTweeningAndComplete(answearText);
        answearText.color = specs.textColor;
    }

    private void CheckIfTweeningAndComplete(object target)
    {
        if(DOTween.IsTweening(target))
        {
            DOTween.Complete(target);
        }
    }



    public bool ResolveValidity()
    {
        if (targetValidityState == ValidityState.Uncertain)
            return true;

        TransitionTo(new SuperState(currentInteractionState, targetValidityState));

        return ((targetValidityState == ValidityState.Right &&
                 currentInteractionState == InteractionState.Selected) ||
                (targetValidityState == ValidityState.Wrong &&
                 currentInteractionState == InteractionState.Idle));
    }

    #endregion

}

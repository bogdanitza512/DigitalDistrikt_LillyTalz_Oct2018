﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Treatment.Reward.Miscellaneous;
using DG.Tweening;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using BN512.Extensions;
using System;

/// <summary>
/// 
/// </summary>
public class TreatmentReward : MonoBehaviour {

    #region Nested Types

    #endregion

    #region Fields and Properties

    public TreatmentRewardManager rewardManager;

    public State currentState = State.Locked;

    public Image blueFill;

    public Image greenFill;

    public float targetValue;

    public List<Image> images = new List<Image>();


    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    [Button(ButtonSizes.Medium)]
    public void AutoPopulateFields()
    {
        foreach (Transform child in transform)
        {

            if (child.name == "Image_Fill_Blue")
            {
                blueFill = child.GetComponent<Image>();
            }

            if (child.name == "Image_Fill_Green")
            {
                greenFill = child.GetComponent<Image>();
            }
        }
    }

    public void TransitionTo(State state)
    {
        if(currentState != state)
        {
            if (Application.isPlaying)
            {
                TransitionTo_DOTween(rewardManager.transitionDict[state]);
            }
            else
            {
                TransitionTo_NoLerp(rewardManager.transitionDict[state]);
            }

            currentState = state;
        }
    }

    private void TransitionTo_DOTween(StateTransitionSpecs specs)
    {
        var duration = rewardManager.baseDuration * specs.durationMultiplier;

        CheckIfTweeningAndComplete(blueFill);
        blueFill.DOFade(specs.blueFillAlpha, duration)
                 .SetLoops(specs.loops);

        CheckIfTweeningAndComplete(greenFill);
        greenFill.DOFade(specs.greenFillAlpha, duration)
                 .SetLoops(specs.loops);
        
    }

    private void TransitionTo_NoLerp(StateTransitionSpecs specs)
    {
        CheckIfTweeningAndComplete(blueFill);
        blueFill.color = 
            blueFill.color.WithAlpha(specs.blueFillAlpha);

        CheckIfTweeningAndComplete(greenFill);
        greenFill.color = 
            greenFill.color.WithAlpha(specs.greenFillAlpha);

    }

    private void CheckIfTweeningAndComplete(object target)
    {
        if (DOTween.IsTweening(target))
        {
            DOTween.Complete(target);
        }
    }

    public void OnReward_Click()
    {
        if(currentState == State.Unlocked)
        {
            CollectReward();
            rewardManager.NotifyRewardCollected();
            currentState = State.Locked;
        }

    }

    private void CollectReward()
    {
        if(!images.IsEmpty())
        {
            foreach (Image img in images)
            {
                img.DOFade(targetValue,
                           rewardManager.baseDuration)
                   .SetLoops(3,LoopType.Yoyo);
            }
        }
    }

    #endregion

}

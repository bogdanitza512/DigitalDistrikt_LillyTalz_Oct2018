﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Treatment.Reward.Miscellaneous;
using Sirenix.OdinInspector;
using UnityEngine.UI;
using DG.Tweening;
using BN512.Extensions;

/// <summary>
/// 
/// </summary>
public class TreatmentRewardManager : SerializedMonoBehaviour {

    #region Nested Types



    #endregion

    #region Fields and Properties

    public TreatmentGameController gameCtrl;

    public List<TreatmentReward> rewards;

    public Dictionary<State, StateTransitionSpecs> transitionDict =
        new Dictionary<State, StateTransitionSpecs>();
    
    public float baseDuration = 1;

    public Image spinnerCicle;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    [Button(ButtonSizes.Medium)]
    public void AutoPopulate()
    {
        rewards.Clear();

        foreach(Transform child in transform)
        {
            var reward = child.GetComponent<TreatmentReward>();
            if(reward != null)
            {
                rewards.Add(reward);
                reward.rewardManager = this;
                reward.AutoPopulateFields();
                reward.TransitionTo(State.Locked);
            }
        }
    }

    public void RevealReward(int index)
    {
        if(rewards.Count > index)
            rewards[index].TransitionTo(State.Unlocked);
    }

    public void BurstSpinner()
    {
        DOTween.Sequence()
               .Append(spinnerCicle
                       .DOFade(1, baseDuration))
               .Join(spinnerCicle.transform
                     .DORotate(spinnerCicle.transform.rotation.eulerAngles + new Vector3().WithZ(-180),
                               baseDuration * 2).SetLoops(2, LoopType.Incremental).SetEase(Ease.Linear))
               .AppendInterval(baseDuration)
               .Append(spinnerCicle.DOFade(0,baseDuration));


        
    }

    internal void NotifyRewardCollected()
    {
        gameCtrl.Notify_RewardCollected();
        BurstSpinner();
    }

    #endregion

}

﻿using System.Collections;
using System.Collections.Generic;
using Game.ConfigMiscellaneous;
using ScreenMgr;
using UnityEngine;
using BN512.Extensions;
using System;
using System.Linq;

/// <summary>
/// 
/// </summary>
public class TreatmentGameController : MonoBehaviour {

    #region Nested Types



    #endregion

    #region Fields and Properties

    public GameConfig config;

    public GameMode gameMode;

    public ScreenManager screenManager;

    public TreatmentQuizzManager quizzManager;

    public TreatmentCountdownManager countdownManager;

    public TreatmentRewardManager rewardManager;

    public int countdownDuration = 25;

    public int currentQuestionIndex = 0;

    public int score = 0;
    public int maxScore = 5;

    public QuizzQuestionContent CurrentQuestion
    {
        get
        {
            return config.TreatmentQuestions[currentQuestionIndex];
        }

    }

    public Queue<int> askedQuestions = new Queue<int>();

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

	public void StartGame()
    {
        LoadNewQuestion();
    }

    public void EndGame()
    {
        countdownManager.Hide();
        screenManager.HideAll();
    }

    public void RestartGame()
    {
        gameMode.RestartScene();
    }

    public void Notify_AnswearComputed(bool validity)
    {
        countdownManager.StopCountdown();
        if(validity == true)
        {
            score++;
            rewardManager.RevealReward(score - 1);
        }
        lastQuestionValidity = validity;
    }

    public void Notify_RewardCollected()
    {
        if (score == maxScore)
        {
            EndGame();
        }else
        {
            LoadNewQuestion();
        }
    }

    public void Notify_CountdownFinished()
    {
        LoadNewQuestion();
    }

    bool lastQuestionValidity;

    public void Notify_QuestionAnsweard()
    {
        
        if (score != maxScore && 
            lastQuestionValidity == false)
        {
            LoadNewQuestion();
        }

    }

    public void LoadNewQuestion()
    {
        countdownManager.StopCountdown();

        if (askedQuestions.Count != 0)
        {
            /*
            //currentQuestionIndex =
            var unaskedQuestion =
            config.TreatmentQuestions
                  .Where((obj) =>
                        !askedQuestions.Contains(
                                        config
                                        .TreatmentQuestions
                                        .IndexOf(obj)))
                  .ToList();
            print("UnAsked Question: [{0}]".FormatWith(
            string.Join(", ",
                        unaskedQuestion
                            .ConvertAll((input) => config.TreatmentQuestions.IndexOf(input).ToString())
                            .ToArray())));
            //.GetRandomIndex();
            currentQuestionIndex = unaskedQuestion.GetRandomIndex();
            print("New Unique question: {0}".FormatWith(currentQuestionIndex));
            */

            var randomQA =
                config.TreatmentQuestions
                      .Where((qa) => !askedQuestions.Contains(config.TreatmentQuestions.IndexOf(qa)))
                      .ToList()
                      .GetRandomItem();
            
            currentQuestionIndex = config.TreatmentQuestions.IndexOf(randomQA);

        }
        else
        {
            currentQuestionIndex =
                config.TreatmentQuestions.GetRandomIndex();
        }

        askedQuestions.Enqueue(currentQuestionIndex);

        if (askedQuestions.Count >= maxScore * 2)
        {
            askedQuestions.Dequeue();
        }

        /*
        print("Asked Question: [{0}]".FormatWith(
            string.Join(", ", 
                        askedQuestions
                            .ToList()
                            .ConvertAll((input) => input.ToString())
                            .ToArray())));
                            */

        quizzManager.LoadCurrentQuestion();

        countdownManager.StartCountdown();
    }
    #endregion

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine.UI;
using TMPro;
using BN512.Extensions;
using System;

/// <summary>
/// 
/// </summary>
public class TreatmentCountdownManager : MonoBehaviour {

    #region Nested Types



    #endregion

    #region Fields and Properties

    public TreatmentGameController gameCtrl;

    public CanvasGroup canvasGroup;

    [SerializeField]
    Image emptyBar;

    [SerializeField]
    Image fillBar;

    [SerializeField]
    TMP_Text timeLeft;

    [SerializeField]
    string timeLeftFormat;

    [SerializeField]
    int currentTime = 15;

    public int CurrentTime
    {
        get { return currentTime; }
        set
        {
            if (value != currentTime)
            {
                currentTime = value;
                timeLeft.text =
                    timeLeftFormat.FormatWith(currentTime);
            }
        }
    }

    Sequence countdownSequence;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    public void StartCountdown()
    {
        fillBar.fillAmount = 1;
        CurrentTime = gameCtrl.countdownDuration;

        countdownSequence =
        DOTween.Sequence()
               .Append(fillBar.DOFillAmount(0, gameCtrl.countdownDuration)
                                   .SetEase(Ease.Linear))
               .Join(DOTween.To(() => { return CurrentTime; },
                                (x) => { CurrentTime = x; },
                                0,
                                gameCtrl.countdownDuration)
                            .SetEase(Ease.Linear))
               .OnStart(() => { currentTime = gameCtrl.countdownDuration; })
               .OnComplete(() => { gameCtrl.Notify_CountdownFinished(); });
    }

    public void StopCountdown()
    {
        if (countdownSequence != null && countdownSequence.IsPlaying())
            countdownSequence.Kill();
        Blink();
    }

    public void Hide()
    {
        canvasGroup.DOFade(0, .5f);
    }

    public void Reveal()
    {
        canvasGroup.DOFade(1, .5f);
    }

    public void Blink()
    {
        canvasGroup.DOFade(1, 1f)
                   .SetLoops(2, LoopType.Yoyo);
    }

    #endregion

}

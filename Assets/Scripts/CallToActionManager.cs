﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.Events;

/// <summary>
/// 
/// </summary>
public class CallToActionManager : MonoBehaviour
{

    #region Nested Types



    #endregion

    #region Fields and Properties

    public float holdDuration = 25;

    public bool isHandDown = false;

    public Image rightHand;
    public Image leftHand;

    public UnityEvent onHoldComplete;

    Sequence fillSequence;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {

    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {

    }

    #endregion

    #region Methods



    public void OnHand_Down()
    {
        // rightHand.fillAmount = 0;
        // leftHand.fillAmount = 0;

        if (!isHandDown)
        {
            isHandDown = true;

            fillSequence = DOTween.Sequence()
                                  .Append(rightHand.DOFillAmount(1, holdDuration - holdDuration * rightHand.fillAmount)
                                                   .SetEase(Ease.Linear))
                                  .Join(leftHand.DOFillAmount(1, holdDuration - holdDuration * leftHand.fillAmount)
                                                .SetEase(Ease.Linear))
                                  .OnComplete(onHoldComplete.Invoke)
                                  .Play()
                                  ;

        }
    }

    public void OnHand_Up()
    {
        if(isHandDown)
        {
            isHandDown = false;

            if (fillSequence.IsActive() || fillSequence.IsPlaying())
                fillSequence.PlayBackwards();
        }
    }

    #endregion

}

﻿using System.Collections;
using System.Collections.Generic;
using BN512.Extensions;
using DG.Tweening;
using UnityEngine;

/// <summary>
/// 
/// </summary>
public class RotatingMovement : MonoBehaviour
{

    #region Nested Types



    #endregion

    #region Fields and Properties

    public Transform objectToRotate;
    public float fullCycleDuration = 2.5f;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {

    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {

    }

    #endregion

    #region Methods

    void StartRotating()
    {
        objectToRotate.transform.DORotate(objectToRotate.transform.eulerAngles.WithY(360),
                                          fullCycleDuration)
                        .SetEase(Ease.Linear)
                        .SetLoops(-1, LoopType.Incremental);
    }

    #endregion

}
